<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('register');
    }

    public function welcome(Request $request){
        $namaDepan = $request->input('nama');
        $namaBelakang = $request->input('nama_b');

        return view('welcome',["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
