<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'daftar']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('partial/table');
});

Route::get('/data-table', function(){
    return view('partial/datatable');
});

//CRUD CAST
//CREATE, route yang mengarah ke form tambah data cast
Route::get('/cast/create', [castController::class, 'create']);
//Route untuk insert ke database tabel cast
Route::post('/cast', [castController::class, 'store']);

//Read, route untuk menampilkan semua data table cast di DB
Route::get('/cast', [castController::class, 'index']);
Route::get('/cast/{id}', [castController::class, 'show']);

//Update, route yang mengarah ke form edit data dengan params id
Route::get('/cast/{id}/edit', [castController::class, 'edit']);
Route::put('/cast/{id}', [castController::class, 'update']);

//Delete
Route::delete('/cast/{id}', [castController::class, 'destroy']);