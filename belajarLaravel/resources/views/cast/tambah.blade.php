@extends('layout/master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="mb-3">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" >
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label>Umur</label>
        <input type="text" name="umur" class="form-control @error('umur') is-invalid @enderror" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label>Bio</label>
        <textarea name="bio" id="bio" class="form-control @error('bio') is-invalid @enderror" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection