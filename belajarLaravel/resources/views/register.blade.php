@extends('layout/master')
@section('judul')
    Form Registration
@endsection
@section('content')
<form action="/welcome" method="POST">
  @csrf
<label for="nama">First Name:</label> <br /><br />
<input type="text" name="nama" /> <br /><br />
<label for="nama_b">Last Name:</label><br /><br />
<input type="text" name="nama_b" /><br /><br />
<label for="gender">Gender:</label> <br /><br />
<input type="radio" name="gender" />Male <br />
<input type="radio" name="gender" />Female <br />
<input type="radio" name="gender" />Other <br />
<br />
<label for="negara">Nationality:</label><br /><br />
<select name="negara" id="negara">
  <option value="indonesia">Indonesian</option>
  <option value="singapura">Singapuran</option>
  <option value="malaysia">Malaysian</option>
  <option value="australia">Australian</option>
</select>
<br /><br />
<label for="bahasa">Language Spoken:</label><br /><br />
<input type="checkbox" />Bahasa Indonesia <br />
<input type="checkbox" />English <br />
<input type="checkbox" />Other <br /><br />
<label for="bio">Bio:</label> <br /><br />
<textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br />
<button type="submit">Sign Up</button>
</form>
@endsection

